[unreleased]
  - Add linker script
  - Add never ending loop with wait for interruption
  - Add Hello, world!
  - Add use of global_asm! macro

[0.1.0]
  - Initial commit
