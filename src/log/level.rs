use super::stream::Streamable;
use super::stream::OStream;
use super::sgr::SGR;
use super::sgr::Colour;

#[derive(Copy,Clone)]
pub enum Level {
    Error,
    Warn,
    Info,
    Debug,
    Trace,
}

impl Level {
    pub fn colour(&self) -> Colour {
        match self {
            Level::Error => Colour::Red,
            Level::Warn  => Colour::Yellow,
            Level::Info  => Colour::White,
            Level::Debug => Colour::Cyan,
            Level::Trace => Colour::Custom{red: 200, green: 200, blue: 200},
        }
    }
    pub fn str(&self) -> &'static str {
        match self {
            Level::Error => "Error",
            Level::Warn  => "Warn",
            Level::Info  => "Info",
            Level::Debug => "Debug",
            Level::Trace => "Trace",
        }
    }
}
impl Streamable for Level {
    fn send<T: OStream>(self, ostream: &T) {
        ostream.send("[")
               .send(SGR::Foreground(self.colour()))
               .send(self.str())
               .send(SGR::Reset)
               .send("]");
    }
}
