use super::level::Level;
use super::stream::OStream;

struct Uart (*mut u8);

const UART: Uart = Uart(0x09000000 as *mut u8);

impl OStream for Uart {
    fn send_byte(&self, byte: u8) -> &Self {
        unsafe {
            *self.0 = byte;
        }
        self
    }
}

pub fn log(level: Level, string: &str) {
    UART.send(level);
    UART.send(" ");
    UART.send(string);
    UART.send("\n\r");
}

