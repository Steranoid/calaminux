pub trait OStream
{
    fn send_byte(&self, byte: u8) -> &Self;
    fn send<T: Streamable + Sized>(&self, streamable: T) -> &Self where Self: Sized
     {
        streamable.send(self);
        self
    }
}

pub trait Streamable : Sized
{
    fn send<T: OStream>(self, ostream: &T);
}

impl Streamable for &[u8] {
    fn send<T: OStream>(self, ostream: &T) {
        for byte in self {
            ostream.send_byte(*byte);
        }
    }
}

impl Streamable for &str {
    fn send<T: OStream>(self, ostream: &T) {
        ostream.send(self.as_bytes());
    }
}

impl Streamable for u8 {
    fn send<T: OStream>(self, ostream: &T) {
        let rem = self%10;
        let div = self/10;
        if div != 0 {
            ostream.send(div);
        }
        ostream.send_byte(
        match rem {
            0 => '0',
            1 => '1',
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5',
            6 => '6',
            7 => '7',
            8 => '8',
            _ => '9',
        } as u8);
    }
}
