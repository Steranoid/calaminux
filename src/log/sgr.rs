use super::stream::OStream;
use super::stream::Streamable;

#[derive(Copy,Clone)]
pub struct ControlCharacter(&'static str);

pub const ESCAPE : ControlCharacter  = ControlCharacter("\x1b");

impl Streamable for ControlCharacter {
    fn send<T: OStream>(self, ostream: &T) {
        ostream.send(self.0);
    }
}

#[derive(Copy,Clone)]
pub struct FeEscapeSequences(&'static str);

impl Streamable for FeEscapeSequences {
    fn send<T: OStream>(self, ostream: &T) {
        ostream.send(ESCAPE);
        ostream.send(self.0);
    }
}

pub const SS2: FeEscapeSequences = FeEscapeSequences("N");
pub const SS3: FeEscapeSequences = FeEscapeSequences("O");
pub const DCS: FeEscapeSequences = FeEscapeSequences("P");
pub const CSI: FeEscapeSequences = FeEscapeSequences("[");
pub const ST:  FeEscapeSequences = FeEscapeSequences("\\");
pub const OSC: FeEscapeSequences = FeEscapeSequences("]");
pub const SOS: FeEscapeSequences = FeEscapeSequences("X");
pub const PM:  FeEscapeSequences = FeEscapeSequences("^");
pub const APC: FeEscapeSequences = FeEscapeSequences("_");

#[derive(Copy,Clone)]
pub enum Colour {
    Black,
    Bordeau,
    DarkGreen,
    Orange,
    DarkBlue,
    Purple,
    DarkCyan,
    Gray,
    DarkGray,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White,
    Custom{red: u8, green: u8, blue: u8},
}

impl Streamable for Colour {
    fn send<T: OStream>(self, ostream: &T) {
        match self {
            Colour::Black     => ostream.send("5;0"),
            Colour::Bordeau   => ostream.send("5;1"),
            Colour::DarkGreen => ostream.send("5;2"),
            Colour::Orange    => ostream.send("5;3"),
            Colour::DarkBlue  => ostream.send("5;4"),
            Colour::Purple    => ostream.send("5;5"),
            Colour::DarkCyan  => ostream.send("5;6"),
            Colour::Gray      => ostream.send("5;7"),
            Colour::DarkGray  => ostream.send("5;8"),
            Colour::Red       => ostream.send("5;9"),
            Colour::Green     => ostream.send("5;10"),
            Colour::Yellow    => ostream.send("5;11"),
            Colour::Blue      => ostream.send("5;12"),
            Colour::Magenta   => ostream.send("5;13"),
            Colour::Cyan      => ostream.send("5;14"),
            Colour::White     => ostream.send("5;15"),
            Colour::Custom{red, green, blue} => {
                ostream.send("2;")
                       .send(red)
                       .send(";")
                       .send(green)
                       .send(";")
                       .send(blue)
            }
        };
    }
}

#[derive(Copy,Clone)]
pub enum SGR {
    Reset,
    Bold,
    Faint,
    Italic,
    Underline,
    SlowBlink,
    RapidBlink,
    ReverseVideo,
    Conceal,
    CrossedOut,
    DefaultFont,
    AlternativeFont(u8),
    Fraktur,
    DoublyUnderline,
    NormalIntensity,
    Foreground(Colour),
    ResetForeground,
    Background(Colour),
    ResetBackground,
}

impl Streamable for SGR {
    fn send<T: OStream>(self, ostream: &T) {
        ostream.send(CSI);
        match self {
            SGR::Reset               => ostream.send("0"),
            SGR::Bold                => ostream.send("1"),
            SGR::Faint               => ostream.send("2"),
            SGR::Italic              => ostream.send("3"),
            SGR::Underline           => ostream.send("4"),
            SGR::SlowBlink           => ostream.send("5"),
            SGR::RapidBlink          => ostream.send("6"),
            SGR::ReverseVideo        => ostream.send("7"),
            SGR::Conceal             => ostream.send("8"),
            SGR::CrossedOut          => ostream.send("9"),
            SGR::DefaultFont         => ostream.send("10"),
            SGR::AlternativeFont(i)  => ostream.send(i+10),
            SGR::Fraktur             => ostream.send("20"),
            SGR::DoublyUnderline     => ostream.send("21"),
            SGR::NormalIntensity     => ostream.send("22"),
            SGR::Foreground(c)       => {
                ostream.send("38;")
                       .send(c)
            },
            SGR::ResetForeground     => ostream.send("39"),
            SGR::Background(c)       => {
                ostream.send("48;")
                       .send(c)
            },
            SGR::ResetBackground     => ostream.send("49"),
        }.send("m");
    }
}
