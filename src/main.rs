#![no_std]
#![no_main]

pub mod log;

use core::panic::PanicInfo;
use core::arch::global_asm;
use log::Level;

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {}
}

global_asm!(r#"
    .globl _start
    .type   _start, %function
    .extern LD_STACK_PTR

    .section ".text.boot"

_start:
    ldr x30, =LD_STACK_PTR
    mov sp, x30
    bl _main

NEVER_ENDING_LOOP:
    wfi
    b NEVER_ENDING_LOOP
    "#
);

#[no_mangle]
pub extern "C" fn _main() {
    log::log(Level::Error, "This is an error");
    log::log(Level::Warn, "This is a warning");
    log::log(Level::Info, "This is an info");
    log::log(Level::Debug, "This is a debug");
    log::log(Level::Trace, "This is a trace");
}
